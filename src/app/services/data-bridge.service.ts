import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataBridgeService {
  dataDetails: any = [];

  private dataSource = new  BehaviorSubject(this.dataDetails);
  currentData = this.dataSource.asObservable();

  constructor() { }
  
  sendData(data: any) {
    this.dataSource.next(data)
  }
}
