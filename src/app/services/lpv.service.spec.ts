import { TestBed } from '@angular/core/testing';

import { LpvService } from './lpv.service';

describe('LpvService', () => {
  let service: LpvService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LpvService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
