import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { firstValueFrom, Observable, tap } from 'rxjs';
import { LaisserPasser } from '../model/laisser-passer';

const HTTP_OPTIONS = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};


@Injectable({
  providedIn: 'root'
})
export class LpvService {

  readonly API_URL = "http://localhost:8081";
  readonly ENDPOINT_COUNTRY = "/api/lpvtab/";

  constructor(private httpClient : HttpClient) { }

  save(body: LaisserPasser): Observable<LaisserPasser> {
    console.log(this.API_URL + this.ENDPOINT_COUNTRY);
    return this.httpClient.post<LaisserPasser>(this.API_URL + this.ENDPOINT_COUNTRY, body, HTTP_OPTIONS)
    .pipe(tap((res: any) => {}, (err: any) => {})
    );
  }

  async save2(body: LaisserPasser): Promise<LaisserPasser> {
    const response = await firstValueFrom(this.httpClient.post<LaisserPasser>(this.API_URL + this.ENDPOINT_COUNTRY, body ,HTTP_OPTIONS));
      if (!response) {
        console.log('something was wrong');
      }
      return response;
  }


  
}
