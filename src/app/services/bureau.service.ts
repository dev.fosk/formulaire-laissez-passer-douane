import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BureauService {

  readonly API_URL = "http://localhost:8081";
  readonly ENDPOINT_CUO = "/api/edouanes/bureau"

  constructor(private httpClient : HttpClient) { }

  getBureau(){
    return this.httpClient.get(this.API_URL + this.ENDPOINT_CUO);
  }
}
