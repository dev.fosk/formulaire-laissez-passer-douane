import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PaysService {
 
  //attributs
  readonly API_URL = "http://localhost:8081";
  readonly ENDPOINT_COUNTRY = "/api/edouanes/pays"
  
  //Constructeur
  constructor(private httpClient : HttpClient) { }

  //Methodes
  getPays(){
    return this.httpClient.get(this.API_URL + this.ENDPOINT_COUNTRY);
  }

}
