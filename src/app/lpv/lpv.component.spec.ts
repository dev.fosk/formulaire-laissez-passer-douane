import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LpvComponent } from './lpv.component';

describe('LpvComponent', () => {
  let component: LpvComponent;
  let fixture: ComponentFixture<LpvComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LpvComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LpvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
