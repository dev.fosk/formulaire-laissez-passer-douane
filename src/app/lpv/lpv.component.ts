import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LaisserPasser } from '../model/laisser-passer';
import { BureauService } from '../services/bureau.service';
import { DataBridgeService } from '../services/data-bridge.service';
import { LpvService } from '../services/lpv.service';
import { PaysService } from '../services/pays.service';

@Component({
  selector: 'app-root',
  templateUrl: './lpv.component.html',
  styleUrls: ['./lpv.component.css'],
  providers: [PaysService, BureauService, LpvService]
})

export class LpvComponent implements OnInit {

  title = 'formUsager';
  pays: any;
  bureaux: any;

  current: LaisserPasser;
  constructor(
    private route: ActivatedRoute,
    private paysService: PaysService,
    private bureauService: BureauService,
    private bridgeService: DataBridgeService,
    private lpvService: LpvService,
    private router: Router
  ) {
    this.current = new LaisserPasser();
  }

  ngOnInit(): void {
    this.paysService.getPays().subscribe(datas => {
      this.pays = datas;
    });

    this.bureauService.getBureau().subscribe(datas => {
      this.bureaux = datas;
    });
  }

  onSubmit() {
    this.lpvService.save(this.current).subscribe(result => this.gotoPaiement(result));
  }

  gotoPaiement(result : object) {
    console.log(result);
    this.bridgeService.sendData(result);
    this.router.navigate(['/showdetails']);
  }
}
