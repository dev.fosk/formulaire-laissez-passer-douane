export class LaisserPasser {
    dateHeureOperation!: Date;
    dateMc!: Date;
    dateNais!: Date;
    numEnreg!: number;
    adresseBen!: string;
    adresseComplete!: string;
    adresseEtr!: string;
    charge!: string;
    chassis!: string;
    codeBureau!: string;
    codePaysConducteur!: string;
    codePaysVehicule!: string;
    couleur!: string;
    destination!: string;
    email!: string;
    genre!: string;
    immatriculation!: string;
    libelleBureau!: string;
    libellePaysConducteur!: string;
    libellePaysVehicule!: string;
    lieuNais!: string;
    mailBen!: string;
    mailEtr!: string;
    marque!: string;
    moteur!: string;
    nomConducteur!: string;
    nomDestination!: string;
    nomProprietaire!: string;
    nomProvenance!: string;
    numCi!: string;
    numPasseport!: string;
    numPermis!: string;
    payAdresse!: string;
    payNom!: string;
    payPrenoms!: string;
    prenomsConducteur!: string;
    prenomsProprietaire!: string;
    profession!: string;
    provenance!: string;
    serEnreg!: string;
    statutOperation!: string;
    telBen!: string;
    telephone!: string;
    telEtr!: string;
    dateAst!: Date;
    dateDebut!: Date;
    dateEnreg!: Date;
    dateFin!: Date;
    datQuit!: Date;
    montant!: number;
    id!: number;
    numAst!: number;
    yearAst!: number;
    agentAdresse!: string;
    agentNom!: string;
    agentPrenoms!: string;       
    anneeEnreg!: string;
    numQuit!: string;
    quitSer!: string;
    serAst!: string;
    tcv!: string;
    tit!: string;
    ts!: string;
    tv!: string;
    typeLPV !: string;
}
