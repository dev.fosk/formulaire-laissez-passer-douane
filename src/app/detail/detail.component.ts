import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataBridgeService } from '../services/data-bridge.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  receiptData: any;

  constructor(
    private route: ActivatedRoute,
    private bridgeService: DataBridgeService
  ){

  }
  ngOnInit() {
    //const lpv = this.route.snapshot.;
    //console.log(lpv);

    // Get data from parent component
    this.bridgeService.currentData.subscribe((data: any) => {
      this.receiptData = data;
      console.log(data);
    });
  }
}
