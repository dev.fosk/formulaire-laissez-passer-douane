import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { DetailComponent } from './detail/detail.component';
import { LpvComponent } from './lpv/lpv.component';

const routes: Routes = [
  { path: 'showdetails', component: DetailComponent },
  { path: '', component: LpvComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
