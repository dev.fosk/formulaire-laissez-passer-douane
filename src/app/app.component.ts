import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { PaysService } from './services/pays.service';
import { LpvService as LaissezPasserService } from './services/lpv.service';
import { LaisserPasser } from './model/laisser-passer';
import { BureauService } from './services/bureau.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [PaysService, BureauService, LaissezPasserService]
})

export class AppComponent implements OnInit{
  
  title = 'formUsager';
  pays : any;
  bureaux : any;
  
  current : LaisserPasser;
  
  vehiculeFormGroup = this._formBuilder.group({
    pays: ['', Validators.required],
    immatriculation: ['', Validators.required],
    dmc: ['', Validators.required],
    chassis: ['', Validators.required],
    moteur: ['', Validators.required],
    marque: ['', Validators.required],
    genre: ['', Validators.required],
    charge: ['', Validators.required],
    couleur: ['', Validators.required],
    proprietaire: this._formBuilder.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      profession: ['', Validators.required],
      adressComplete: ['', Validators.required],
      telephone: ['', Validators.required],
      mail: ['', Validators.required],
    })
  });

  conductorFormGroup = this._formBuilder.group({
    nationalte: ['', Validators.required],
    numeroPermis: ['', Validators.required],
    numeroPassport: ['', Validators.required],
    numeroCNI: ['', Validators.required],
    nom: ['', Validators.required],
    prenom: ['', Validators.required],
    dateNaissance: ['', Validators.required],
    lieuNaissance: ['', Validators.required],
    adressComplete: ['', Validators.required],
    telephone: ['', Validators.required],
    mail: ['', Validators.required],
    adressCompleteExterieur: ['', Validators.required],
    telephoneExterieur: ['', Validators.required],
    mailExterieur: ['', Validators.required],
  });

  freeDriveFormGroup = this._formBuilder.group({
    codeBureau: ['', Validators.required],
    libelleBureau: ['', Validators.required],
    numeroEnregistrement: ['', Validators.required],
    dateEnregistrement: ['', Validators.required],
    numeroLiq: ['', Validators.required],
    dateLiq: ['', Validators.required],
    numeroQuit: ['', Validators.required],
    dateQuit: ['', Validators.required],
    montant: ['', Validators.required],
    information: this._formBuilder.group({
      typeLPV: ['', Validators.required],
      debutPeriode: ['', Validators.required],
      finPeriode: ['', Validators.required],
      destination: ['', Validators.required],
      provenance: ['', Validators.required],
      tit: ['', Validators.required],
      tcv: ['', Validators.required],
      tel: ['', Validators.required],
      td: ['', Validators.required]
    }),
    etablissementQuittance: this._formBuilder.group({
      nom: ['', Validators.required],
      prenomPayeur: ['', Validators.required],
      adress: ['', Validators.required],
    })
  });

  constructor(
    private route: ActivatedRoute,
    private _formBuilder: FormBuilder, 
    private paysService : PaysService, 
    private bureauService : BureauService, 
    private laissezPasserService : LaissezPasserService,
    private router: Router
  ) {
    this.current = new LaisserPasser();
  }

  ngOnInit(): void {
    console.log('on Init ....');
    this.paysService.getPays().subscribe( datas => {
      this.pays = datas;
    });

    this.bureauService.getBureau().subscribe( datas => {
      this.bureaux = datas;
    });
  }

  onSubmit(){
    this.laissezPasserService.save(this.current).subscribe(result => this.gotoPaiement());
  }

  //gotoPaiement( lpv : LaisserPasser){
    gotoPaiement( ){
  /*
    const navigationExtras: NavigationExtras = {
      state: {
        objectParam: lpv
      }
    };
    */
    this.router.navigate(['/showdetails']);
  }
}
